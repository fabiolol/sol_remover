![Scheme](examples/front.jpg)
# sol_remover

See https://github.com/Kegi91/sol_remover for the original project.

sol_remover is a Python script that removes all the specified residues
from the given .gro file that are in a specific range. The intended use
is to remove water molecules from the inside of the membrane. This is done
by calculating the average of the z components of the specified atom
both for the upper and the lower leaflet. All the water molecules that are 
between the leaflets are then removed.

It works with python 3 and it needs the following libraries:

```
optparse
subprocess
shutil
re
os
sys
```


If you wish you can copy it into your gromacs folder:

```
chmod u+x sol_remover
cp sol_remover /gromacs-path/bin/
```



```
For Martini systems:
sol_remover -f solv.gro  -o solv_fix.gro --rn W --ra W --rs 1 --ln POPC --la PO4  -p topol.top

For All atoms systems:
sol_remover -f solv.gro  -o solv_fix.gro --rn SOL --ra OW --rs 3 --ln POPC --la P  -p topol.top
```
